Config              = {}
Config.DrawDistance = 1000.0
Config.MaxDelivery	= 10
Config.TruckPrice	= 0
Config.Locale       = 'en'

Config.Trucks = {
	"tribike2",
	--"packer"	
}

Config.Cloakroom = {
	CloakRoom = {
			Pos   = {x = -262.781, y = -901.545, z = 31.511},
			Size  = {x = 2.0, y = 2.0, z = 1.0},
			Color = {r = 000, g = 204, b = 204},
			Type  = 27
		},
}

Config.Zones = {
	VehicleSpawner = {
			Pos   = {x = -268.681, y = -890.349, z = 29.92},
			Size  = {x = 2.0, y = 2.0, z = 1.0},
			Color = {r = 000, g = 204, b = 204},
			Type  = 27
		},

	VehicleSpawnPoint = {
			Pos   = {x = -269.304, y = -893.32, z = 30.32},
			Size  = {x = 2.0, y = 2.0, z = 1.0},
			Type  = -1
		},
}

Config.Livraison = {
-------------------------------------------Los Santos
	-- Strawberry avenue et Davis avenue
	Delivery1LS = {
			Pos   = {x = 57.71, y = -1003.602, z = 28.257},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},
	-- a coté des flic
	Delivery2LS = {
			Pos   = {x = 382.906, y = -1024.497, z = 28.437},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},
	-- vers la plage
	Delivery3LS = {
			Pos   = {x = 805.549, y = -810.122, z = 25.300},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},
	-- studio 1
	Delivery4LS = {
			Pos   = {x = 487.279, y = -1295.717, z = 28.426},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},
	-- popular street et el rancho boulevard
	Delivery5LS = {
			Pos   = {x = 136.65, y = -1278.667, z = 28.560},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},
	--Alta street et las lagunas boulevard
	Delivery6LS = {
			Pos   = {x = -232.504, y = -1310.439, z = 30.196},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},
	--Rockford Drive Noth et boulevard del perro
	Delivery7LS = {
			Pos   = {x = -286.431, y = -1061.845, z = 26.105},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},
	--Rockford Drive Noth et boulevard del perro
	Delivery8LS = {
			Pos   = {x = -59.164, y = -616.27, z = 36.257},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},
	--New empire way (airport)
	Delivery9LS = {
			Pos   = {x = 135.681, y = -1030.954, z = 28.254},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},
	--Rockford drive south
	Delivery10LS = {
			Pos   = {x = 308.049, y = -727.913, z = 28.217},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},

	Delivery11LS = {
			Pos   = {x = -33.992, y = -1677.09, z = 28.583},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},

	Delivery12LS = {
			Pos   = {x = 286.135, y = -936.978, z = 28.516},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},

	Delivery13LS = {
			Pos   = {x = -520.634, y = -854.997, z = 29.407},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},

	Delivery14LS = {
			Pos   = {x = 76.414, y = -870.701, z = 30.609},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},

	Delivery15LS = {
			Pos   = {x = 460.995, y = -770.243, z = 26.458},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},

	Delivery16LS = {
			Pos   = {x = 286.716, y = -1147.791, z = 28.392},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},

	Delivery17LS = {
			Pos   = {x = -42.982, y = -1108.861, z = 25.938},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},

	Delivery18LS = {
			Pos   = {x = -693.754, y = -811.041, z = 23.115},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},
------------------------------------------- Blaine County
	-- panorama drive
	Delivery1BC = {
			Pos   = {x = 478.339, y = -978.157, z = 26.884},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},
	-- route 68
	Delivery2BC = {
			Pos   = {x = 494.735, y = -571.01, z = 23.479},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},
	-- Algonquin boulevard et cholla springs avenue
	Delivery3BC = {
			Pos   = {x = 435.109, y = -647.473, z = 27.636},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},
	-- Joshua road
	Delivery4BC = {
			Pos   = {x = 168.053, y = -567.264, z = 42.773},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},
	-- East joshua road
	Delivery5BC = {
			Pos   = {x = 102.058, y = -818.458, z = 30.240},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},
	-- Seaview road
	Delivery6BC = {
			Pos   = {x = 105.618, y = -933.328, z = 28.694},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},
	-- Paleto boulevard
	Delivery7BC = {
			Pos   = {x = -228.025, y = 1118.248, z = 21.921},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},
	-- Paleto boulevard et Procopio drive
	Delivery8BC = {
			Pos   = {x = -306.23, y = -1179.96, z = 22.877},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},
	-- Marina drive et joshua road
	Delivery9BC = {
			Pos   = {x = -320.582, y = -1389.775, z = 35.400},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},
	-- Pyrite Avenue
	Delivery10BC = {
			Pos   = {x = -229.187, y = -1377.724, z = 30.158},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},
	-- Pyrite Avenue 2
	Delivery11BC = {
			Pos   = {x = 240.41, y = -1379.715, z = 32.642},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},

	Delivery12BC = {
			Pos   = {x = 371.452, y = -1608.763, z = 28.392},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},

	Delivery13BC = {
			Pos   = {x = -293.876, y = -601.781, z = 32.653},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},

	Delivery14BC = {
			Pos   = {x = -599.15, y = -929.984, z = 22.963},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},

	Delivery15BC = {
			Pos   = {x = 298.454, y = -584.295, z = 42.361},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},

	Delivery16BC = {
			Pos   = {x = 459.377, y = -857.479, z = 26.409},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},
	
	Delivery17BC = {
			Pos   = {x = 101.01, y = -1115.426, z = 28.803},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},

	Delivery18BC = {
			Pos   = {x = -97.274, y = -1013.909, z = 26.375},
			Color = {r = 204, g = 204, b = 204},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 204},
			Type  = 27,
			Paye = 60
		},

	RetourCamion = {
			Pos   = {x = -273.128, y = -902.781, z = 30.217},
			Color = {r = 204, g = 200, b = 000},
			Size  = {x = 2.0, y = 2.0, z = 1.0},
			Color = {r = 204, g = 000, b = 000},
			Type  = 27,
			Paye = 0
		},
	
	AnnulerMission = {
			Pos   = {x = -274.0, y = -905.336, z = 30.217},
			Color = {r = 204, g = 000, b = 000},
			Size  = {x = 2.0, y = 2.0, z = 1.0},
			Color = {r = 204, g = 000, b = 000},
			Type  = 27,
			Paye = 0
		},
}