USE `essentialmode`;

INSERT INTO `addon_account` (name, label, shared) VALUES
  ('society_dtaxi','DTaksi',1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
  ('society_dtaxi','DTaksi',1)
;

INSERT INTO `jobs` (name, label) VALUES
  ('DTaksi','DTaksi')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
  ('DTaksi',0,'recrue','Harjoittelija',200,'{}','{}'),
  ('DTaksi',1,'novice','Kokenut',300,'{}','{}'),
  ('DTaksi',2,'experimente','Ammattilainen',350,'{}','{}'),
  ('DTaksi',3,'uber','Uber',400,'{}','{}'),
  ('DTaksi',4,'boss','Johtaja',600,'{}','{}')
;
